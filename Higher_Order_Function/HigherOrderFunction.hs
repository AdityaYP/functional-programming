import Data.List

myLength ls = sum (map (ubahSatu) ls)
    where ubahSatu el = 1

iter :: Int -> (a -> a) -> a -> a
iter 0 f x = x
iter n f x = f (iter (n-1) f x)

sumOfSquares n = foldr (+) 0 (map (squares) [1 .. n])
    where squares a = a*a

myFlip :: (a -> b -> c) -> b -> a -> c
myFlip f a b = f b a

mystery [] [] = []
mystery (x:xs) (y:ys) = (x+y) : map (+x) ys 

splits []         = [([],[])]
splits (x:xs)     = ([],x:xs) : [(x:ps,qs) | (ps,qs)<-splits xs]

perms [] = [[]]
perms ls = [ x:ps | x <- ls, ps <- perms (ls \\ [x]) ]

maxTiga :: Integer -> Integer -> Integer -> Integer
maxTiga a b c = max a (max b c)

sumList ls = foldr (+) 0 ls

-- Latihan pembuatan fold untuk abstraksi code yang berulang

data Buyer = Family Int Int Int
        | Single
        | WithFriends Int
        | Member Buyer
        | Company Int
    deriving (Show)

hargaTiket satuan (Family x y z) = x*satuan + y*satuan - 100
hargaTiket satuan (Single) = satuan
hargaTiket satuan (WithFriends n) = satuan*n - 50
hargaTiket satuan (Member b) = (hargaTiket satuan b) - 200
hargaTiket satuan (Company n) = n * satuan - 150

hitungPenonton (Family x y z) = x+y+z
hitungPenonton (Single) = 1
hitungPenonton (WithFriends n) = n
hitungPenonton (Member b) = hitungPenonton b
hitungPenonton (Company n) = n

-- hargaTiket dan hitungPenonton memiliki kemiripan, dapat diabstraksi dengan fold

foldBuyer (f,s,w,m,c) (Family x y z) = f x y z
foldBuyer (f,s,w,m,c) (Single) = s
foldBuyer (f,s,w,m,c) (WithFriends n) = w n
foldBuyer (f,s,w,m,c) (Member b) = m (foldBuyer (f,s,w,m,c) b)
foldBuyer (f,s,w,m,c) (Company n) = c n

newHargaTiket satuan buyer = foldBuyer (tf,ts,tw,tm,tc) buyer
    where 
        tf x y z = satuan*x + satuan*y - 100
        ts = satuan
        tw n = satuan * n - 50
        tm x = x - 200
        tc n = satuan * n - 150

newHitungPenonton buyer = foldBuyer (tf,ts,tw,tm,tc) buyer
    where
        tf x y z = x+y+z
        ts = 1
        tw n = n
        tm x = x
        tc n = n

-- kita tidak perlu pemanggilan rekursif pada definisi fungsi untuk Member, karena sudah dibungkus di foldBuyer

data Expr = C Float
        | Expr :+ Expr
        | Expr :- Expr
        | Expr :* Expr
        | Expr :/ Expr
        | V String
        | Let String Expr Expr

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2 
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
evaluate (V v) = 0