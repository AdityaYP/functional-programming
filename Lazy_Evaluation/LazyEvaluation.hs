import Data.List

add [] [] = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibs = 1 : 1 : add fibs (tail fibs)

-- tail adalah fungsi untuk mengambil list index 1 sampai terakhir
-- terdapat fungsi lain yaitu init, head, last

perm [] = [[]]
perm ls = [ x:ps | x <- ls, ps <- perm(ls\\[x])]

-- ls\\[x] adalah untuk melihat difference
-- misal [1,2,3] \\ [2] = [1,3]

divisor n = [x| x <- [1 .. n], n `mod` x == 0]

pythaTriple = [(x,y,z)| z <- [5 .. ], y <- [2 .. z-1], x <- [2 .. y-1], x*x + y*y == z*z]

-- dalam list comprehension di haskell kita harus memperhatikan variable di input set
-- meskipun secara teori sudah benar
-- dalam kasus diatas harus z terlebih dahulu
-- dapat terjadi infinite loop jika salah meletakkan 

sumList [] = 0
sumList (x:xs) = x + sumList xs