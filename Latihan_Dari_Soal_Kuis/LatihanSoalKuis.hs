-- Nomor 1
maxTiga :: Int -> Int -> Int -> Int
maxTiga a b c = maxi a (maxi b c)
    where maxi x y = if x>= y then x else y

-- -- Kita bisa menggunakan fungsi max yang sudah ada di haskell
maxTiga2 :: Int -> Int -> Int -> Int
maxTiga2 a b c = max a (max b c)

-- Nomor 2
nomor2 = [ (x,y) | x <- [1 .. 4], y <- [2 .. 6], x * 2 == y ]

-- -- Hasilnya adalah [(1,2),(2,4),(3,6)]

-- Nomor 3
quickSort :: [Integer] -> [Integer]
quickSort [] = []
quickSort (x:xs) = quickSort [y|y<-xs,y<x] ++ [x] ++ quickSort [y|y<-xs,y>=x]

-- Nomor 4
jumlahListKanan ls = foldr (+) 0 ls
jumlahListKiri ls = foldl (+) 0 ls

-- -- Kedua fungsi diatas menghasilkan hasil yang sama
-- -- Lalu kenapa ada dua foldr dan foldl? 
-- -- Karena terkadang ada kasus yang membuat efisiensi dari foldr dan foldl berbeda
-- -- dalam kasus nomor 4 kebetulan sama

-- Nomor 5
misteri xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

-- Nomor 6
primes = sieve [2..]
    where
        sieve (x:xs) = x:sieve[y|y<-xs,y `mod` x /= 0]

-- Nomor 7
myflip :: (a -> b -> c) -> b -> a -> c
myflip f a b = f b a