import Data.List

animalFriends :: [(String, String)]
animalFriends = [ ("Kuda", "Singa")
                , ("Singa", "Ayam")
                , ("Ayam", "Sapi")
                , ("Sapi", "Jerapah") ]

animalFriendLookup :: [(String, String)] -> Maybe String
animalFriendLookup animalMap =
  case lookup "Kuda" animalMap of
       Nothing -> Nothing
       Just kudaFriend ->
         case lookup kudaFriend animalMap of
              Nothing -> Nothing
              Just kudaFriendFriend -> 
                case lookup kudaFriendFriend animalMap of
                  Nothing -> Nothing
                  Just kudaFriendFriendFriend -> Just kudaFriendFriendFriend


monadicFriendLookup :: [(String, String)] -> Maybe String
monadicFriendLookup animalMap = lookup "Kuda" animalMap
    >>= (\kudaFriend -> lookup kudaFriend animalMap
    >>= (\kuda2ndFriend -> lookup kuda2ndFriend animalMap
    >>= (\friend -> Just friend)))

sugaryFriendLookup :: [(String, String)] -> Maybe String
sugaryFriendLookup animalMap = do
    kudaFriend    <- lookup "Kuda" animalMap
    kudaFriend'   <- lookup kudaFriend animalMap
    kudaFriend''  <- lookup kudaFriend' animalMap
    return kudaFriend''